import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoFormWithValidatorsExplicitComponent } from './demo-form-with-validators-explicit.component';

describe('DemoFormWithValidatorsExplicitComponent', () => {
  let component: DemoFormWithValidatorsExplicitComponent;
  let fixture: ComponentFixture<DemoFormWithValidatorsExplicitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemoFormWithValidatorsExplicitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoFormWithValidatorsExplicitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
